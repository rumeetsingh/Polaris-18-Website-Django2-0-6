# Polaris-18-Website-Django2.0.6 #
# TEAM SIGNIFY #
# TECHFEST WEBSITE #

- - - - 

## Requirements (Python==3.6.4) ##

* Django==2.0.6
* django-crispy-forms==1.7.2
* django-heroku==0.3.1
* gunicorn==19.8.1
* Pillow==5.1.0
* psycopg2==2.7.5
* whitenoise==3.3.1
* cloudinary==1.12.0

## Features ##

* Responsive multi-page web design.(All pages automatically adjusts according to device dimensions)
* Attention grabbing Home page with movable animations.
* Login/Signup system for visitors to interact with page. Features like Forgot Password and Change Password are also available.
* Forums for asking questions regarding anything related to Tech Fest. Comments to questions also have an Up-Vote option.
* If any staff member posts a comment, verification mark will appear next to his name.  
* Events/Workshop page where visitors can register online for any event/workshop happening in Tech Fest.
* Everything happening in Tech Fest can be found in one click, by visiting Explore more on Home page.
* Admins have power to add Updates, Events/Workshops, Sponsors, Team Members etc.

### Image Upload Resolutions

| Image  | Resolution(w*h) | Ratio | P/O |
|--------------|---------------|-----|-----------|
| Team Member  | 500px * 500px | 1:1 | Preferred |
| Sponsor      | 500px * 500px | 1:1 | Preferred |
| Event Poster | 579px * 386px | 3:2 | Optional  | 

### How to style event description ###

* Open [WordToHtml](https://wordtohtml.net/)
* Type your text in the Visual Editor.
* Copy the styling from HTML Editor.
* Paste the text in the event description panel in Django Administration.

## Contributors ##

* [Django](https://www.djangoproject.com/) - Backend Language
* [Bootstrap](https://getbootstrap.com/) - Styling
* [Cloudinary](https://cloudinary.com/) - Media Files Management
* [Gmail](https://www.google.com/gmail/about/) - Email Management

- - - - 
[Heroku Deployment Link](https://signifycoding.herokuapp.com/)
- - - - 

### Facebook Share Button

* With the help of facebook share button, all the UPDATES and EVENTS can be directly shared to Polaris'18 facebook page.

> FACEBOOK SHARE BUTTON ONLY WORKS ON HEROKU DEPLOYED VERSION. IT WILL NOT WORK ON LOCAL SERVER