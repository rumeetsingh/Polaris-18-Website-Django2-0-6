from django.contrib import admin
from users.models import TeamMember ,UserProfile
# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
    search_fields = ['user' , 'first_name' , 'last_name' , 'college' , 'department' , 'current_year']
    list_display = ['user' , 'first_name' , 'last_name' , 'college' , 'department' , 'current_year']
    class Meta():
        model = UserProfile

class TeamMemberAdmin(admin.ModelAdmin):
    list_display = ['name' , 'number']
    list_editable = ['number']
    class Meta():
        model = TeamMember

admin.site.register(TeamMember , TeamMemberAdmin)
admin.site.register(UserProfile , UserProfileAdmin)
