from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from cloudinary.models import CloudinaryField

# Create your models here.
class TeamMember(models.Model):
    name = models.CharField(max_length=300)
    #image = models.ImageField(upload_to="members")
    image = CloudinaryField('image')
    number = models.IntegerField(unique=True)

    def __str__(self):
        return self.name

class UserProfile(models.Model):
   user = models.OneToOneField(User , on_delete=models.CASCADE)
   first_name = models.CharField(max_length=300 , blank=False)
   last_name = models.CharField(max_length=300 , blank=False)
   college = models.CharField(max_length=300 , blank=False)
   department = models.CharField(max_length=300 , blank=False)
   YEAR = (
        ('School', 'School'),
        ('1st', 'B.tech First'),
        ('2nd', 'B.tech Second'),
        ('3rd', 'B.tech Third'),
        ('4th', 'B.tech Forth'),
        ('M-Tech', 'M.tech'),
        ('PhD', 'PhD'),
        ('Others', 'Others'),
            )
   current_year = models.CharField(max_length=10, choices=YEAR , blank=False)
