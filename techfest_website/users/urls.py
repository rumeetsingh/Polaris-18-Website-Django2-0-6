from django.contrib import admin
from django.urls import path
from users import views , urls

app_name = 'users'

urlpatterns = [
    path('user_register/', views.register, name='user_register'),
    path('user_login/' , views.user_login , name = 'user_login'),
    path('user_logout/' , views.user_logout , name = 'user_logout'),
    path('user_cant_login/ujqCmCpa122w/' , views.login_fail , name = 'login_fail'),
    path('profile/' , views.profile , name = 'profile'),
    path('password_change/', views.change_password, name='change_password'),
    ]
