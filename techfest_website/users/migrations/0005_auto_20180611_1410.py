# Generated by Django 2.0.6 on 2018-06-11 08:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20180611_1410'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='current_year',
            field=models.CharField(choices=[('School', 'School'), ('B.tech First', 'B.tech First'), ('B.tech Second', 'B.tech Second'), ('B.tech Third', 'B.tech Third'), ('B.tech Forth', 'B.tech Forth'), ('M-Tech', 'M.tech'), ('PhD', 'PhD')], max_length=20),
        ),
    ]
