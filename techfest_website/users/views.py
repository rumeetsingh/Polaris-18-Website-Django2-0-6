from django.shortcuts import render
from users.forms import SignUpForm , SignUpProfileForm
# imports for login
from django.contrib.auth import authenticate , login , logout
from django.http import HttpResponseRedirect , HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from users.models import TeamMember , UserProfile
# imports for profile
from events.models import Registration
#import for password change
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm



def index(request):
    members = TeamMember.objects.order_by('number')
    return render(request , 'index.html' , { 'members' : members })

def explore(request):
    return render(request , 'explore.html' , {})

def error_500(request):
    return render(request , 'error_404.html')

def error_404(request):
    return render(request , 'error_404.html')

def register(request):
    registered = False
    user_form = SignUpForm(request.POST or None)
    profile_form = SignUpProfileForm(request.POST or None)
    if user_form.is_valid() and profile_form.is_valid():
        user = user_form.save()
        password = user_form.cleaned_data['password']
        user.set_password(password)
        user.save()

        profile = profile_form.save(commit=False)
        profile.user = user
        profile.save()
        registered = True

    return render(request , 'users/register.html' , { 'registered' : registered , 'user_form' : user_form , 'profile_form' : profile_form })

def login_fail(request):
    return render(request , 'users/login_fail.html' , {})

def user_login(request):
    failed = False
    if request.method=='POST' :
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(username = username , password = password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse("index"))
            else:
                return HttpResponse("ACCOUNT NOT ACTIVE")
        else:
            return HttpResponseRedirect('/users/user_cant_login/ujqCmCpa122w/')
            failed = True
    else:
        return render(request , 'users/login.html' , { 'failed' : failed })

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))

@login_required
def profile(request):
    regs = Registration.objects.filter(user = request.user).order_by('time')
    profile = UserProfile.objects.get(user = request.user)
    return render(request , 'users/profile.html' , { 'regs' : regs , 'profile' : profile , })

@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return HttpResponseRedirect('/users/profile/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })
