from django import forms
from django.contrib.auth.models import User
from users.models import UserProfile

class SignUpForm(forms.ModelForm):
    username = forms.CharField(help_text=False)
    email = forms.EmailField(required=True)
    password = forms.CharField(widget = forms.PasswordInput() ,label='Password:')
    confirm_password = forms.CharField(widget=forms.PasswordInput() , label='Confirm Password:')

    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data['password']
        confirm_password = self.cleaned_data['confirm_password']
        if password != confirm_password:
            raise forms.ValidationError(" Password and Confirm Password does not match!")
        user = User.objects.filter(username=username)
        if user :
            raise forms.ValidationError("This username is already taken!")

    class Meta():
        model = User
        fields = ['username' , 'email' , 'password' , 'confirm_password']

class SignUpProfileForm(forms.ModelForm):

    class Meta():
        model = UserProfile
        fields = [  'first_name' , 'last_name' , 'college' , 'department' , 'current_year' ]
