from django.contrib import admin
from django.urls import path
from events import views , urls

app_name = 'events'

urlpatterns = [
    path('', views.events, name='events_all'),
    path('event_detail/<int:id>/' , views.event_detail , name='event_detail'),
    path('event_payment/' , views.payment , name='payment')
    ]
