# Generated by Django 2.0.6 on 2018-06-09 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0010_auto_20180609_2039'),
    ]

    operations = [
        migrations.AddField(
            model_name='reg',
            name='text',
            field=models.CharField(default='hello', max_length=300),
        ),
    ]
