# Generated by Django 2.0.6 on 2018-06-09 16:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0012_remove_reg_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='reg',
            name='registered',
            field=models.BooleanField(default=False),
        ),
    ]
