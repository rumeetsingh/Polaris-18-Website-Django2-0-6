# Generated by Django 2.0.6 on 2018-06-12 13:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0021_remove_registration_registered'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='fee',
            field=models.IntegerField(default=0),
        ),
    ]
