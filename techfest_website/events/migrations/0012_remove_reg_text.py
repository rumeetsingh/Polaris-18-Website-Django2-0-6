# Generated by Django 2.0.6 on 2018-06-09 15:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0011_reg_text'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reg',
            name='text',
        ),
    ]
