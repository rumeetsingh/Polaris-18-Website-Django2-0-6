# Generated by Django 2.0.6 on 2018-06-10 03:20

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('events', '0015_auto_20180610_0848'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='reg',
            unique_together={('event', 'user')},
        ),
    ]
