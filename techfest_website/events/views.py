from events.models import Event , Registration
from django.shortcuts import render , get_object_or_404
from events.forms import RegForm
from django.http import HttpResponseRedirect , HttpResponse
from django.contrib.auth.decorators import login_required
# Create your views here.
def events(request):
    events = Event.objects.order_by('start')
    return render(request , 'events/events.html' , { 'events' : events })

def event_detail(request , id ):
    event = get_object_or_404(Event , id = id )
    if not request.user.is_authenticated:
        regs = Registration.objects.order_by('user')
    else:
        regs = Registration.objects.filter(event=event , user=request.user).order_by('user')

    reg_form = RegForm()
    if request.method == 'POST':
        reg_form = RegForm(request.POST)
        if reg_form.is_valid():
            form = reg_form.save(commit=False)
            form.user = request.user
            form.event = event
            form.save()
            return HttpResponseRedirect(request.path)
    return render(request , 'events/event_detail.html' , { 'event' : event , 'form' : reg_form , 'regs' : regs ,  })

def payment(request):
    return render(request, 'events/payment.html')
