from django.db import models
from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from cloudinary.models import CloudinaryField

# Create your models here.
class Type(models.Model):
    type = models.CharField(max_length=300)

    def __str__(self):
        return self.type


class Event(models.Model):
    name = models.CharField(max_length=300)
    type = models.ForeignKey(Type , on_delete=models.CASCADE)
    image = CloudinaryField('image')
    description = models.TextField()
    fee = models.IntegerField(default=0)
    eligibility = models.CharField(max_length=300 , default="Everyone")
    venue = models.CharField(max_length=2000)
    start = models.DateTimeField(auto_now=False, auto_now_add=False)
    end = models.DateTimeField(auto_now=False, auto_now_add=False)
    registeration_open = models.BooleanField(default=False)
    #google_form_url = models.URLField(max_length=200 , null=True , blank=True , default='www.example.com')

    def __str__(self):
        return self.name

class Registration(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL , default=1 ,on_delete=models.CASCADE)
    event = models.ForeignKey(Event , on_delete=models.CASCADE)
    fee_status = models.BooleanField(default=False)
    time = models.DateTimeField(auto_now_add=True,auto_now=False)

    class Meta():
        unique_together = ('event', 'user')
