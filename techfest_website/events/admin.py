from django.contrib import admin
from events.models import Type , Event , Registration
# Register your models here.


class RegistrationAdmin(admin.ModelAdmin):

    list_display = ['event' , 'user' , 'fee_status' , 'time']
    list_filter = ['event']
    class Meta():
        model = Registration

class EventAdmin(admin.ModelAdmin):
    list_display = ['name' , 'fee' , 'registeration_open']
    class Meta():
        model = Event

admin.site.register(Registration , RegistrationAdmin)
admin.site.register(Type)
admin.site.register(Event , EventAdmin)
