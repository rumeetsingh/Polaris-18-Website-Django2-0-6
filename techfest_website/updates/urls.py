from django.contrib import admin
from django.urls import path
from updates import views , urls

app_name = 'updates'

urlpatterns = [
    path('', views.updates, name='updates_all'),
    path('update_detail/<int:id>/' , views.update_detail , name='update_detail'),
    ]
