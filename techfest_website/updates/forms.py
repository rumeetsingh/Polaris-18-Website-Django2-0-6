from django import forms
from updates.models import Post , Saying

class SayingForm(forms.ModelForm):
    text = forms.CharField(label='Comment:' , widget=forms.Textarea(attrs={'rows':2,}))
    class Meta():
        model = Saying
        fields = ['text']
