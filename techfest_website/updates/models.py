from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from cloudinary.models import CloudinaryField
# Create your models here.

class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL , default=1 ,on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True,auto_now=False)
    title = models.CharField(max_length=1000)
    text = models.TextField()
    image = CloudinaryField('image')

    def __str__(self):
        return self.title

class Saying(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL , default=1 ,on_delete=models.CASCADE)

    to_post = models.ForeignKey(Post , on_delete=models.CASCADE)
    #content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    #object_id = models.PositiveIntegerField()
    #content_object = GenericForeignKey('content_type', 'object_id')

    time = models.DateTimeField(auto_now_add=True,auto_now=False)
    text = models.TextField()

    def __str__(self):
        return self.text
