from django.contrib import admin
from updates.models import Post , Saying
from updates.forms import SayingForm
# Register your models here.
admin.site.register(Post)
admin.site.register(Saying)
