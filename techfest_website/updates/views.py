from django.shortcuts import render , get_object_or_404
from updates.models import Post , Saying
from updates.forms import SayingForm
from django.http import HttpResponseRedirect
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
# Create your views here.

def updates(request):
    recents = Post.objects.order_by('-time')[:10]
    contact_list = Post.objects.order_by('-time')
    paginator = Paginator(contact_list, 7) # Show 7 contacts per page
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request , 'updates/updates.html' , { 'posts' : posts , 'recents' : recents })

def update_detail(request , id ):
    post = get_object_or_404(Post , id = id )
    comments = Saying.objects.filter(to_post = post).order_by('time')
    commentform = SayingForm(request.POST or None)
    if commentform.is_valid():
        cform = commentform.save(commit=False)
        cform.user = request.user
        cform.to_post = post
        cform.save()
        return HttpResponseRedirect( request.path )
    return render(request , 'updates/update_detail.html' , { 'post' : post , 'comments' : comments , 'commentform' : commentform })
