"""techfest_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path , include
from users import views as user_views
from forums import views as forum_views
from events import views as event_views
from updates import views as update_views
from sponsors import views as sponsor_views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as django_views
from django.conf.urls import handler404 , handler500


urlpatterns = [
    path('admin_ehsy5cbk3e/', admin.site.urls),
    path('' , user_views.index , name='index'),

    #path('' , include('django.contrib.auth.urls')),
    path('password_reset/', django_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', django_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', django_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', django_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path('explore/' , user_views.explore , name='explore'),
    path('users/', include('users.urls' , namespace = 'users')),
    path('forums/', include('forums.urls' , namespace = 'forums')),
    path('updates/', include('updates.urls' , namespace = 'updates')),
    path('events/' , include('events.urls' , namespace = 'events')),
    path('sponsors/' , sponsor_views.sponsor , name='sponsors'),
]

if settings.DEBUG :
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404= user_views.error_404
handler500= user_views.error_500
