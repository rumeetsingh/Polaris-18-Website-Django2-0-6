from django import forms
from forums.models import Question , Comment , Like


class QuestionForm(forms.ModelForm):
    text = forms.CharField(label='Your Question:' , widget=forms.Textarea(attrs={'rows':4,}))
    class Meta():
        model = Question
        fields = ['text']


class CommentForm(forms.ModelForm):
    text = forms.CharField(label='Your Answer:' , widget=forms.Textarea(attrs={'rows':4,}))
    class Meta():
        model = Comment
        fields = ['text']

class LikeForm(forms.ModelForm):
    class Meta():
        model = Like
        fields = []
