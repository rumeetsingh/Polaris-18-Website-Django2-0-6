from django.shortcuts import render , get_object_or_404
from forums.models import Question , Comment , Like
from forums.forms import QuestionForm , CommentForm , LikeForm
from django.http import HttpResponseRedirect
# Create your views here.

def forums(request):
    questions = Question.objects.order_by('-time')

    questionform = QuestionForm(request.POST or None)
    if questionform.is_valid():
        qform = questionform.save(commit=False)
        qform.user = request.user
        qform.save()
        return HttpResponseRedirect("/forums/")
    return render(request , 'forums/forums.html' , { 'questions' : questions , 'questionform' : questionform ,  })

def forum_edit(request , id):
    question = get_object_or_404(Question , id = id )
    editform = QuestionForm(request.POST or None , instance=question)
    if editform.is_valid():
        eform = editform.save(commit=False)
        eform.save()
        return HttpResponseRedirect("/forums/")
    return render(request , 'forums/forum_edit.html' , { 'question' : question , 'editform' : editform })

def forum_delete(request , id):
    question = get_object_or_404(Question , id = id )
    if request.method=='POST':
        question.delete()
        return HttpResponseRedirect("/forums/")
    return render(request , 'forums/forum_delete.html' , { 'question' : question })

def forum_detail(request , id ):
    question = get_object_or_404(Question , id = id )
    comments = Comment.objects.filter(to_question = question).order_by('time')
    commentform = CommentForm(request.POST or None)
    if commentform.is_valid():
        cform = commentform.save(commit=False)
        cform.user = request.user
        cform.to_question = question
        cform.save()
        return HttpResponseRedirect( request.path )
    return render(request , 'forums/forum_detail.html' , { 'question' : question , 'comments' : comments , 'commentform' : commentform })

def comment_edit(request , id):
    question = get_object_or_404(Comment , id = id )
    editform = CommentForm(request.POST or None , instance=question)
    if editform.is_valid():
        eform = editform.save(commit=False)
        eform.save()
        return HttpResponseRedirect("/forums/")
    return render(request , 'forums/forum_edit.html' , { 'question' : question , 'editform' : editform })

def comment_delete(request , id):
    question = get_object_or_404(Comment , id = id )
    if request.method=='POST':
        question.delete()
        return HttpResponseRedirect("/forums/")
    return render(request , 'forums/forum_delete.html' , { 'question' : question })

def forum_like(request , id):
    comment = get_object_or_404(Comment , id = id)
    likes = Like.objects.filter(to_comment = comment)
    likeform = LikeForm(request.POST or None)

    if not request.user.is_authenticated:
        like_check = Like.objects.order_by('user')
    else:
        like_check = Like.objects.filter(to_comment = comment , user=request.user).order_by('user')

    if likeform.is_valid():
        lform = likeform.save(commit=False)
        lform.user = request.user
        lform.to_comment = comment
        lform.save()
        return HttpResponseRedirect( request.path )
    return render(request , 'forums/forum_like.html' , { 'comment' : comment , 'likeform' : likeform , 'likes' : likes , 'like_check' : like_check ,})
