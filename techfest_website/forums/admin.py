from django.contrib import admin
from forums.models import Question , Comment , Like
# Register your models here.
admin.site.register(Like)
admin.site.register(Question)
admin.site.register(Comment)
