from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# Create your models here.
class Question(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL , default=1 ,on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True,auto_now=False)
    text = models.CharField(max_length=1000 )

    def __str__(self):
        return self.text

class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL , default=1 ,on_delete=models.CASCADE)

    to_question = models.ForeignKey(Question , on_delete=models.CASCADE)
    #content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    #object_id = models.PositiveIntegerField()
    #content_object = GenericForeignKey('content_type', 'object_id')

    time = models.DateTimeField(auto_now_add=True,auto_now=False)
    text = models.TextField()

    def __str__(self):
        return self.text

class Like(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL , default=1 ,on_delete=models.CASCADE)
    to_comment = models.ForeignKey(Comment , on_delete=models.CASCADE)

    class Meta():
        unique_together = ('to_comment', 'user')
