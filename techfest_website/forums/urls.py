from django.contrib import admin
from django.urls import path
from forums import views , urls

app_name = 'forums'

urlpatterns = [
    path('', views.forums, name='forums_all'),
    path('forum_edit/<int:id>/' , views.forum_edit , name='forum_edit'),
    path('forum_delete/<int:id>/' , views.forum_delete , name='forum_delete'),
    path('comment_edit/<int:id>/' , views.comment_edit , name='comment_edit'),
    path('comment_delete/<int:id>/' , views.comment_delete , name='comment_delete'),
    path('forum_detail/<int:id>/' , views.forum_detail , name='forum_detail'),
    path('forum_like/<int:id>/' , views.forum_like , name='forum_like'),
    ]
