from django.contrib import admin
from sponsors.models import Sponsor
# Register your models here.
class SponsorAdmin(admin.ModelAdmin):
    list_display = ['name' , 'number']
    list_editable = ['number']
    class Meta():
        model = Sponsor

admin.site.register(Sponsor , SponsorAdmin)
