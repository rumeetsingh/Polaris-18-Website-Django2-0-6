from django.db import models
from cloudinary.models import CloudinaryField
# Create your models here.
class Sponsor(models.Model):
    name = models.CharField(max_length=300)
    url = models.URLField(max_length=1000 , blank=True)
    image = CloudinaryField('image')
    number = models.IntegerField(unique=True)

    def __str__(self):
        return self.name
