from django.shortcuts import render
from sponsors.models import Sponsor
# Create your views here.

def sponsor(request):
    sponsors = Sponsor.objects.order_by('number')
    return render(request , 'sponsors.html' , { 'sponsors' : sponsors })
